package dk.alfabetacain.snt;

import java.io.IOException;

public class CLI {

    public static void main(String[] args) throws IOException {
        String mode = args[0];

        if (mode.toUpperCase().equals("RECEIVE")) {
            receive(args[1]);
        } else if (mode.toUpperCase().equals("SEND")) {
            send(args[1], args[2], Integer.parseInt(args[3]));
        } else {
            System.out.println("Unknown mode: " + mode);
        }
    }

    private static void receive(String filename) throws IOException {
        try (Connection conn = new Connection()) {
            System.out.println("Port: " + conn.getPort());
            conn.receive(filename);
            conn.close();
        }
    }

    private static void send(String filename, String host, int port) throws IOException {
        try (Connection conn = new Connection()) {
            conn.send(filename, host, port);
            conn.close();
        }
    }
}
