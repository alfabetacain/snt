package dk.alfabetacain.snt;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Connection implements Closeable {

    private static final int BUFFER_SIZE = 1024 * 64;
    private final ServerSocket serverSocket;

    public Connection() throws IOException {
        serverSocket = new ServerSocket(0);
    }

    public int getPort() {
        return serverSocket.getLocalPort();
    }

    public void receive(String filename) throws IOException {
        try (Socket socket = serverSocket.accept()) {
            InputStream in = socket.getInputStream();
            try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(filename)), BUFFER_SIZE)) {
                byte[] buffer = new byte[BUFFER_SIZE];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                out.flush();
            }
            socket.shutdownOutput();
        }
    }

    public void send(String filename, String host, int port) throws IOException {
        try (Socket socket = new Socket(host, port)) {
            File file = new File(filename);
            try (FileInputStream in = new FileInputStream(file)) {
                byte[] buffer = new byte[BUFFER_SIZE];
                BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream(), BUFFER_SIZE);
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                out.flush();
            }
            socket.shutdownOutput();
        }
    }

    public void close() throws IOException {
        serverSocket.close();
    }
}
